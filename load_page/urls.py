from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from .views import *


urlpatterns = [
    path('', index, name='home'),
    path('load_photo', load_photo, name='load_photo'),
    path('get_photo/', get_photo, name='get_photo'),
    path('get_photo/<str:p_hash>/', get_photo, name='get_photo'),
    path('faq/', faq, name='faq'),
    path('donate/', donate, name='donate'),
    path('contacts/', contacts, name='contacts'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
