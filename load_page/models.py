from django.db import models
from django.urls import reverse


class LoadImg(models.Model):
    photo = models.ImageField(
        upload_to='photos/%Y-%m-%d/', verbose_name='Фото'
        )
    time_delete = models.FloatField(verbose_name='Время удаления')
    photo_hash = models.CharField(
        primary_key=True, max_length=255, verbose_name='Хэш'
        )

    def get_absolute_url(self):
        return reverse('get_photo', kwargs={'p_hash', self.photo_hash})
